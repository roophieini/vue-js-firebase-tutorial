export function Meetup (payload, key) {
  this.imageUrl = payload.imageUrl
  this.title = payload.title
  this.date = payload.date
  this.location = payload.location
  this.desc = payload.desc
  this.id = key
}

export function ClearMeetup (payload) {
  this.meetUp = new Meetup(payload)
  delete this.meetUp.id
  return this.meetUp
}
