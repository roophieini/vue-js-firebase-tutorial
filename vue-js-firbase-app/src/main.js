import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import AppHome from './components/Shared/AppHome.vue'
import router from './router'
import * as firebase from 'firebase'
import AlertComp from './components/Shared/Alert.vue'
import './stylus/main.styl'
import { store } from './store/store'
import DateFilter from './filters/dateFilter'

Vue.use(Vuetify)
Vue.component('app-alert', AlertComp)
Vue.component('app-home', AppHome)
Vue.filter('filterDate', DateFilter)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyDOjp_5tr9wfaC4mf1RwbCyDXYTXZNYvYk',
      authDomain: 'vujs-firebase-tutorial.firebaseapp.com',
      databaseURL: 'https://vujs-firebase-tutorial.firebaseio.com',
      projectId: 'vujs-firebase-tutorial',
      storageBucket: 'vujs-firebase-tutorial.appspot.com'
    })
    this.$store.dispatch('loadMeetUps')
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSingIn', user)
      }
    })
  }
})
