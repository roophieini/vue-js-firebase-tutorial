export default {
  changeSideNav (state, payload) {
    state.sideNav = payload
  },
  saveMeetup (state, meetup) {
    state.meetups.push(meetup)
  },
  saveUserData (state, user) {
    state.user = user
  },
  disableLoginAndAddLogout (state, logout) {
    state.menuItems.filter(l => {
      if (l.title === 'Login') {
        l.showAuth = false
      }
    })
    if (!state.menuItems().includes(logout)) {
      this.menuItems.push(logout)
    }
  },
  setLoading (state, payload) {
    state.loading = payload
  },
  setErrorMsg (state, error) {
    state.error = error
  },
  clearError (state) {
    state.error = null
  },
  setMeetups (state, payload) {
    state.meetups = payload
  }
}
