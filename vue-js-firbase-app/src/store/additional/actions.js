import * as firebase from 'firebase'
import { ClearMeetup, Meetup } from '../../objects/meetup'

export default {
  showHideNavbar ({ commit }, payload) {
    commit('changeSideNav', payload)
  },
  loadMeetUps ({ commit }, actions) {
    commit('setLoading', true)
    firebase.database().ref('meetups').once('value')
            .then(res => {
              const meetups = []
              const obj = res.val()
              for (let key in obj) {
                meetups.push(new Meetup(obj[key], key))
              }
              commit('setMeetups', meetups)
              commit('setLoading', false)
            })
            .catch(error => {
              commit('setLoading', false)
              console.log(error)
            })
  },
  saveMeetup ({ commit }, payload) {
    const meetup = new ClearMeetup(payload)
    delete meetup.id
    firebase.database().ref('meetups').push(meetup).then((res) => {
      console.log(res)
      const key = res.key
      meetup.id = key
      commit('saveMeetup', meetup)
    }).catch(error => {
      console.log(error)
    })
  },
  signUp ({ commit }, payload) {
    commit('setLoading', true)
    commit('clearError')
    firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
            .then(user => {
              commit('setLoading', false)
              const newUser = {
                id: user.uid,
                registeredMeetups: []
              }
              commit('saveUserData', newUser)
            }).catch(error => {
              commit('setLoading', false)
              commit('setErrorMsg', error.message)
              console.log(error)
            })
  },
  addMenuItem ({ commit }) {
    commit('disableLoginAndAddLogout', { icon: 'lock_outline', title: 'Logout', link: '/logout', showAuth: true })
  },
  signIn ({ commit }, payload) {
    commit('setLoading', true)
    commit('clearError')
    firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
            .then(user => {
              commit('setLoading', false)
              const newUser = {
                id: user.uid,
                registeredMeetups: []
              }
              commit('saveUserData', newUser)
            }).catch(error => {
              commit('setLoading', false)
              commit('setErrorMsg', error.message)
              console.log(error)
            })
  },
  setError ({ commit }, error) {
    commit('setErrorMsg', error)
  },
  clearError ({ commit }) {
    commit('clearError')
  },
  logoutUser ({ commit }) {
    firebase.auth().signOut().then(l => {
      commit('saveUserData', null)
    }).catch(error => { console.log(error.message) })
  },
  autoSingIn ({ commit }, user) {
    commit('saveUserData', {id: user.uid, registeredMeetups: []})
  }
}
