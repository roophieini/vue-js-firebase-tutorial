export default {
  getMenu (state) {
    return state.menuItems
  },
  getMenuItems (state, getters) {
    return (auth) => {
      if (auth) {
        return getters.getMenu.filter(item => {
          return item.showAuth === true
        })
      } else {
        return getters.getMenu.filter(item => {
          return item.showAuth === false
        })
      }
    }
  },
  getSideNavShow (state) {
    return state.sideNav
  },
  getMeetUps (state) {
    return state.meetups.sort((a, b) => {
      return a.date > b.date
    }
    )
  },
  getMeetup (state) {
    return (meetupId) => {
      return state.meetups.find((meetup) => {
        return meetup.id === meetupId
      })
    }
  },
  getFeaturedMeetups (state, getters) {
    return getters.getMeetUps.slice(0, 5)
  },
  getUser (state) {
    return state.user
  },
  getError (state) {
    return state.error
  },
  getLoading (state) {
    return state.loading
  },
  getParalax (state) {
    return state.paralax
  },
  getParalaxUrl (state) {
    return state.paralaxUrl
  },
  getLogoutSection (state) {
    return state.logout
  },
  userIsLoggedIn (state) {
    return (state.user !== null && state.user !== undefined)
  }
}
