export default {
  sideNav: false,
  menuItems: [
    {icon: 'pageview', title: 'View Meetups', link: '/meetups', showAuth: true},
    {icon: 'add_location', title: 'Organize Meetup', link: '/meetup/create', showAuth: true},
    {icon: 'person', title: 'Profile', link: '/user/profile', showAuth: true},
    {icon: 'lock_outline', title: 'Logout', link: '/Logout', showAuth: true},
    {icon: 'person_add', title: 'Sign up', link: '/signup', showAuth: false},
    {icon: 'lock_open', title: 'Login', link: '/signin', showAuth: false}
  ],
  meetups: [],
  paralaxUrl: 'https://raw.githubusercontent.com/vuetifyjs/parallax-starter/master/template/assets/hero.jpeg',
  paralax: {
    imageUrl: '',
    desc: 'Welcome to the Meetup Service!',
    misc: 'to Create or Register for Meetup!',
    add: 'Scroll down to Explore some of the Meetups!',
    buttonSu: 'Sign Up!',
    buttonSi: 'Sign In!',
    buttonEx: 'Explore Meetups!'
  },
  logout: {
    desc: 'Successful Logged out!',
    add: 'Thank you for using our Meetup Application!',
    buttonSu: 'Homepage!'
  },
  user: null,
  loading: false,
  error: null
}
