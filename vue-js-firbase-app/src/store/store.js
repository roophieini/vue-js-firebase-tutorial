import Vue from 'vue'
import Vuex from 'vuex'
import state from './additional/state.js'
import getters from './additional/getters.js'
import mutations from './additional/mutations.js'
import actions from './additional/actions'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

