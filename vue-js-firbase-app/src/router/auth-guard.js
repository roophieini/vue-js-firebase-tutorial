import { store } from '../store/store'

export default (to, from, next) => {
  if (store.getters.getUser) {
    next()
  } else {
    store.dispatch('setError', 'Unauthorized! Please Login or Sign In!')
    next('/')
  }
}
