import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import ParalaxHome from '@/components/Paralax'
import Logout from '@/components/User/Logout'
import Meetups from '@/components/Meetup/Meetups'
import CreateMeetup from '@/components/Meetup/CreateMeetup'
import Profile from '@/components/User/Profile'
import Signin from '@/components/User/SignIn'
import Signup from '@/components/User/SignUp'
import Meetup from '@/components/Meetup/Meetup'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ParalaxHome',
      component: ParalaxHome
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/meetups',
      name: 'Meetups',
      component: Meetups,
      beforeEnter: AuthGuard
    },
    {
      path: '/meetups/:id',
      name: 'Meetup',
      props: true,
      component: Meetup,
      beforeEnter: AuthGuard

    },
    {
      path: '/meetup/create',
      name: 'CreateMeetup',
      component: CreateMeetup,
      beforeEnter: AuthGuard
    },
    {
      path: '/user/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/signin',
      name: 'Signin',
      component: Signin
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout,
      beforeEnter: AuthGuard
    },
    {
      path: '*',
      redirect: '/'
    }
  ],
  mode: 'history'
})
